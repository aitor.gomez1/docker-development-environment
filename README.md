# Docker development environment
Docker environment that merges the different images needed for the project.

## Login in the gitlab docker register to access the images
```bash
docker login registry.gitlab.com
```

## Build the image
It will take several minutes.

```bash
docker-compose build --force-rm --pull
```

## Run the environment
```bash
docker-compose up -d
```
